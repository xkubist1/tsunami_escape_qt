//#define GL_GLEXT_PROTOTYPES

#include <QtGui/QOpenGLBuffer>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/QOpenGLFunctions>
#include "lib3ds/file.h"
#include "lib3ds/mesh.h"
#include <cassert>
#include "qlogging.h"

class CModel3DS
{
public:
    CModel3DS(std:: string filename,
              QOpenGLShaderProgram* program,
              const GLuint positionAttribute,
              const GLuint normalAttribute,
              const GLuint textureAttribute);
    /// Metoda pro vykresleni objektu
    virtual void Draw() const;
    /// Metoda, kterou musite rucne zavola pro vytvoreni VBO
    virtual void CreateVBO();
    virtual ~CModel3DS();
protected:
    /// Pomocna metoda, ktera spocte pocet projuhelniku meshe
    void GetFaces();
    /// Pocet trojuhelniku tvoricich mesh objektu
    unsigned int m_TotalFaces;
    /// Model, ktery je nacten ze souboru
    Lib3dsFile * m_model;

    /// Shader program predany z nadrazene struktury
    QOpenGLShaderProgram *m_program;

    /// VBO
    QOpenGLBuffer *vertexPositionBuffer;
    QOpenGLBuffer *vertexNormalBuffer;
    QOpenGLBuffer *vertexTexCoordBuffer;

    GLuint m_VertexVBO;
    GLuint m_NormalVBO;
    GLuint m_TexCoordVBO;


    /// jmeno promenne pod kterym vystupuje pozice v shaderu
    GLuint m_positionAttribute;
    /// jmeno promenne pod kterym vystupuji normaly
    GLuint m_normalAttribute;
    /// jmeno promenne s texturovacimi souradnicemi
    GLuint m_texcoordAttribute;
};


