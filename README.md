## Tsunami escape

*Projekt do předmětu PG2, Jakub Kubišta.*

---

**Námět**

• Hra bude představovat útěk před vlnou tsunami, která se přibližuje směrem k pozorovateli. 

• Tato arkáda vznikla na základě hry „The Chase“, ve které je cílem stejně jako v této hře prchat co nejdéle před entitou na pozadí. 

• Hráč bude ovládat surfaře, který se bude pohybovat po vodě před touto vlnou rovněž směrem k pozorovateli. 

• Na hladině se budou objevovat překážky (bedny), do kterých hráč může narazit. 

• *Viz. ukázka podobné hry: [odkaz](https://vimeo.com/62941354).*

---

**Logika**

• Cílem hry je tedy prchat co nejdéle před vlnou a nasbírat co nejvíce bodů, které se přičítají na základě trvání hry.

•  Hráč má tři životy, které může ztratit při interakci s bednou nebo vlnou. Pokud hráč příjde o všechny životy, tak se surfař utopí a nastává konec hry.

•  V případě, že hráč ztratí jeden život, tak je na 3 sekundy nesmrtelný, což bude znázorněno blikáním postavy.

---

**Ovládání**

• Pohyb doprava (D).

• Pohyb doleva (A).

• Pohyb dopředu (S).

• Pohyb dozadu (W).

---

**Spuštění hry**

• Bude brzy v provozu.