import QtQuick 2.0
import QtQuick.Dialogs 1.1

FileDialog {
    property string saveFile

    selectFolder: false
    id: fileDialog
    title: "Please choose a file"
    onAccepted: {
        console.log("You chose: " + fileDialog.fileUrls[0])
        openglScene.loadModel(fileDialog.fileUrls[0])
    }

    onRejected: {
        console.log("Canceled")
        Qt.quit()
    }

    Component.onCompleted: visible = true
}
