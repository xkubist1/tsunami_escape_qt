import QtQuick 2.2

Rectangle {
    property alias buttonText: buttonLabel.text

    id: gameButton
    height: 40
    width: 200
    color: "lightgray"
    radius: 5

    Text{
        id:buttonLabel
        text:"New Game"
        font.pixelSize: 20
        font.bold: true
        anchors.centerIn: parent
    }

    states: [
        State {
            name: "light"

            PropertyChanges {
                target: gameButton
                color: "white"
            }
        }
    ]
}
