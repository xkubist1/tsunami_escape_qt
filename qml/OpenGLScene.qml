import QtQuick 2.0
import OpenGLUnderQML 1.0

Scene {
    width : parent.width-100
    height : parent.height-100
    // toto je zatim nepotrebne, slouzi pro animaci

    SequentialAnimation on rotationAngle {
        NumberAnimation { to: 60; duration: 2500; easing.type: Easing.InQuad }
        NumberAnimation { to: 0; duration: 2500; easing.type: Easing.OutQuad }
        loops: Animation.Infinite
        running: true
    }

    focus: true
  //  Keys.onPressed: {
  //      if (event.key === Qt.Key_Left) openglScene.moveLeft();
  //      else if (event.key === Qt.Key_Escape) Qt.quit();
  //  }
}
