/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the demonstration applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.0
import OpenGLUnderQML 1.0
import QtQuick.Controls 1.2

Item {
    visible: true
    width: 1024
    height: 576


    OpenGLScene{
        id: openglScene
    }

    Rectangle{
        width: parent.width
        height: 100;
        color: "darkgray"

        Row{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10

            Loader {
               id: menuLoader
            }

            MenuButton{
                id:loadButton
                buttonText:"Load model"

                MouseArea {
                    anchors.fill: parent

                    hoverEnabled: true

                    onEntered: {
                        loadButton.state = "light"
                    }

                    onExited: {
                        loadButton.state = ""
                    }

                    onClicked: {
                        menuLoader.source = "FileMenu.qml"
                    }
                }
            }

            MenuButton{
                id:quitButton
                buttonText:"Quit"

                MouseArea {
                    anchors.fill: parent

                    hoverEnabled: true

                    onEntered: {
                        quitButton.state = "light"
                    }

                    onExited: {
                        quitButton.state = ""
                    }

                    onClicked: {
                        Qt.quit()
                    }
                }
            }
        }
    }
}


