#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform sampler2D primaryTexture;
uniform sampler2D secondaryTexture;

varying vec2 v_texcoord;


void main()
{
    // vyber barvu pixelu z textury
    //vec3 finalColor = mix(texture2D(primaryTexture, v_texcoord).rgb,
    //                      texture2D(secondaryTexture, v_secondTexcoord).rgb,
    //                      0.5);

    vec4 primaryColor = 0.5 * texture2D(primaryTexture, v_texcoord) + 0.8 * texture2D(secondaryTexture, v_texcoord);
/*    vec4 secondaryColor = texture2D(secondaryTexture, v_secondTexcoord);

    if (secondaryColor.a == 0.0){
        gl_FragColor = vec4(primaryColor.rgb, 1.0);
    } else {
        gl_FragColor = vec4(secondaryColor.rgb, 1.0);
    }
  */
    gl_FragColor = primaryColor;
}
