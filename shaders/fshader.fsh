#ifdef GL_ES
// Set default precision to medium
precision mediump int;
precision mediump float;
#endif

uniform samplerCube primaryTexture;
varying vec3 texcoords;

void main()

{   vec4 sampColor;
    /*
    sampColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);  //If yes, set it to white
    gl_FragColor = 0.5* sampColor + textureCube(primaryTexture, texcoords);
    */

    gl_FragColor = textureCube(primaryTexture, texcoords);
}
