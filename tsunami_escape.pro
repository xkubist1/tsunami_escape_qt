QT += qml quick opengl

HEADERS += \
    scene.h \
    skybox.h \
    cube.h \
    3dmodel.h \
    lib3ds/atmosphere.h \
    lib3ds/background.h \
    lib3ds/camera.h \
    lib3ds/chunk.h \
    lib3ds/chunktable.h \
    lib3ds/chunktable.sed \
    lib3ds/ease.h \
    lib3ds/file.h \
    lib3ds/io.h \
    lib3ds/light.h \
    lib3ds/material.h \
    lib3ds/matrix.h \
    lib3ds/mesh.h \
    lib3ds/node.h \
    lib3ds/quat.h \
    lib3ds/shadow.h \
    lib3ds/tcb.h \
    lib3ds/tracks.h \
    lib3ds/types.h \
    lib3ds/vector.h \
    lib3ds/viewport.h
SOURCES += main.cpp \
    scene.cpp \
    skybox.cpp \
    cube.cpp \
    3dmodel.cpp \
    lib3ds/atmosphere.c \
    lib3ds/background.c \
    lib3ds/camera.c \
    lib3ds/chunk.c \
    lib3ds/ease.c \
    lib3ds/file.c \
    lib3ds/io.c \
    lib3ds/light.c \
    lib3ds/material.c \
    lib3ds/matrix.c \
    lib3ds/mesh.c \
    lib3ds/node.c \
    lib3ds/quat.c \
    lib3ds/shadow.c \
    lib3ds/tcb.c \
    lib3ds/tracks.c \
    lib3ds/vector.c \
    lib3ds/viewport.c
RESOURCES += tsunami_escape.qrc

CONFIG += c++11

target.path = $$[PWD]/tsunami_escape
INSTALLS += target

OTHER_FILES += \
    changes.txt

LIBS += -lopengl32

DISTFILES += \
    lib3ds/.libs/lib3ds-1.3.0.0.dylib \
    lib3ds/.libs/lib3ds-1.3.dylib \
    lib3ds/.libs/lib3ds.dylib \
    lib3ds/.libs/lib3ds.a \
    lib3ds/.deps/atmosphere.Plo \
    lib3ds/.deps/background.Plo \
    lib3ds/.deps/camera.Plo \
    lib3ds/.deps/chunk.Plo \
    lib3ds/.deps/ease.Plo \
    lib3ds/.deps/file.Plo \
    lib3ds/.deps/io.Plo \
    lib3ds/.deps/light.Plo \
    lib3ds/.deps/material.Plo \
    lib3ds/.deps/matrix.Plo \
    lib3ds/.deps/mesh.Plo \
    lib3ds/.deps/node.Plo \
    lib3ds/.deps/quat.Plo \
    lib3ds/.deps/shadow.Plo \
    lib3ds/.deps/tcb.Plo \
    lib3ds/.deps/tracks.Plo \
    lib3ds/.deps/vector.Plo \
    lib3ds/.deps/viewport.Plo \
    lib3ds/.libs/lib3ds.lai \
    lib3ds/types.txt
